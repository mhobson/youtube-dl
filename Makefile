IMAGE = registry.gitlab.com/mhobson/youtube-dl
TAG = $(shell git describe --always --dirty)

.PHONY: all build push tag_latest push_latest run

all: build tag_latest push push_latest

build:
	docker build --no-cache -t $(IMAGE):$(TAG) .

push:
	docker push $(IMAGE):$(TAG)

tag_latest:
	docker tag $(IMAGE):$(TAG) $(IMAGE):latest

push_latest:
	docker push $(IMAGE):latest

clean:
	docker rmi $(IMAGE):$(TAG) &>/dev/null || true
	docker rmi $(IMAGE):latest &>/dev/null || true

run:
	docker run --rm -it $(IMAGE):$(TAG) sh
