FROM alpine:latest AS base
RUN apk update \
 && apk upgrade --no-cache \
 && apk add --no-cache ca-certificates ffmpeg python tzdata \
 && rm -rf /var/cache/apk/* /var/lib/apk/*

FROM base AS build
RUN apk update && apk add --no-cache --upgrade curl gnupg
RUN curl -Lo /tmp/youtube-dl https://yt-dl.org/downloads/latest/youtube-dl
RUN curl -Lo /tmp/youtube-dl.sig https://yt-dl.org/downloads/latest/youtube-dl.sig
RUN gpg --keyserver keyserver.ubuntu.com --recv-keys '7D33D762FD6C35130481347FDB4B54CBA4826A18'
RUN gpg --keyserver keyserver.ubuntu.com --recv-keys 'ED7F5BF46B3BBED81C87368E2C393E0F18A9236D'
RUN gpg --verify /tmp/youtube-dl.sig /tmp/youtube-dl
RUN chmod a+x /tmp/youtube-dl
RUN /tmp/youtube-dl --version

FROM base
RUN mkdir -m a+rwx /downloads
COPY --from=build /tmp/youtube-dl /usr/local/bin/youtube-dl
ENV SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt
ENV TZ=America/Los_Angeles
WORKDIR /downloads
VOLUME ["/downloads"]
ENTRYPOINT ["youtube-dl"]
CMD ["--help"]
